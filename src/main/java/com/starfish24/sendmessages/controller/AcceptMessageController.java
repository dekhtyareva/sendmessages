package com.starfish24.sendmessages.controller;
import com.starfish24.sendmessages.dto.MessageData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

//import javax.validation.Valid;

@Controller
public class AcceptMessageController {

    @PostMapping(value = "/email/{client}")
    public ResponseEntity<String> sendEmails(@PathVariable String client, @Valid @RequestBody MessageData messageData) {
        System.out.println(messageData.getMessageText());
        return ResponseEntity.ok("Successful result");
    }

}