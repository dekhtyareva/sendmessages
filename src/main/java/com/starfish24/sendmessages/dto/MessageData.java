package com.starfish24.sendmessages.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MessageData {
    @NotBlank(message = "field 'to' is required")
    private String to;
    @NotBlank(message = "subject is required")
    private String subject;
    @NotBlank(message = "template is required")
    private String template;
    @NotBlank(message = "order id is required")
    private String orderId;
    private String messageText;

    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        MessageData messageData = (MessageData) obj;
        if(messageData.getMessageText().equals(messageText) && messageData.getOrderId().equals(orderId) && messageData.getSubject().equals(subject) && messageData.getTemplate().equals(template) && messageData.getTo().equals(to)) {
            return true;
        }
        return false;
    }
}
