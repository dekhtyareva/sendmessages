package com.starfish24.sendmessages.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientInfo {
    private String email;
    private String login;
    private String password;

    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        if(obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        ClientInfo clientInfo = (ClientInfo) obj;
        if(clientInfo.getEmail().equals(email) && clientInfo.getLogin().equals(login) && clientInfo.getPassword().equals(password)) {
            return true;
        }
        return false;
    }
}
