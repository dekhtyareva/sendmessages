package com.starfish24.sendmessages.converter;

import com.starfish24.sendmessages.dto.ClientInfo;
import com.starfish24.sendmessages.dto.MessageData;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.*;
import java.util.List;

@Slf4j
public class MessageDataConverter {

    private final String filePath;

    public MessageDataConverter(String filePath) {
        this.filePath = filePath;
    }

    public List<ClientInfo> parseClientOfFile() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
        bufferedReader.readLine();
        List<ClientInfo> clients = new ArrayList<ClientInfo>();
        String line;
        while((line = bufferedReader.readLine()) != null) {
            ClientInfo client = new ClientInfo();
            String [] str = line.split(";");
            client.setLogin(str[0]);
            client.setEmail(str[str.length-2]);
            client.setPassword(str[str.length-1]);
            log.info("Parse client with login = {}, email = {}, password = {}", client.getLogin(), client.getEmail(), client.getPassword());
            clients.add(client);
        }
        return clients;
    }

    public MessageData convertToMessageData(ClientInfo client) {
        String message = "Новый логин для доступа в систему: " + client.getLogin()+ ", новый пароль: " + client.getPassword();
        MessageData messageData = new MessageData(client.getEmail(), "Логин и пароль для доступа в систему", "sample", "sample", message);
        log.info("Convert client with login = {}, email = {}, password = {}, into MessageData with subject = {}, to = {}, messageText = {}", client.getLogin(), client.getEmail(), client.getPassword(), messageData.getSubject(), messageData.getTo(), messageData.getMessageText());
        return messageData;
    }
}
