package com.starfish24.sendmessages;

import com.starfish24.sendmessages.converter.MessageDataConverter;
import com.starfish24.sendmessages.dto.ClientInfo;
import com.starfish24.sendmessages.dto.MessageData;
import com.starfish24.sendmessages.service.MessageDataClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.List;

@SpringBootApplication
public class SendmessagesApplication {

    public static void main(String[] args) throws IOException {
        SpringApplication.run(SendmessagesApplication.class, args);
        String url = args[1];
        MessageDataClient client = new MessageDataClient();
        client.setURL(url);
        MessageDataConverter messageDataConverter = new MessageDataConverter(args[0]);
        List<ClientInfo> clientList = messageDataConverter.parseClientOfFile();
        for (ClientInfo clientInfo: clientList) {
            MessageData messageData = messageDataConverter.convertToMessageData(clientInfo);
            client.sendMessages(messageData);
        }
    }

}
