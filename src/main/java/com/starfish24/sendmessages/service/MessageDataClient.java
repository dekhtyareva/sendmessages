package com.starfish24.sendmessages.service;


import com.starfish24.sendmessages.dto.MessageData;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
@Setter
public class MessageDataClient {

    private final RestTemplate restTemplate;

    private String URL;

    public MessageDataClient() {
        restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
    }

    public MessageData sendMessages(MessageData messageData) throws IOException {
        Map<String, String> params = new HashMap<String, String>();
        params.put("client", "smtp");
        log.info("RestTemplate Request: URL = {}, Body = {}", URL, messageData);
        return restTemplate.postForObject(URL, messageData, MessageData.class, params);
    }

}