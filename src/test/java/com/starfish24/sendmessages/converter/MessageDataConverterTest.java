package com.starfish24.sendmessages.converter;

import com.starfish24.sendmessages.dto.ClientInfo;
import com.starfish24.sendmessages.dto.MessageData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class MessageDataConverterTest {

    private MessageDataConverter messageDataConverter;

    @BeforeEach
    void init() {
        messageDataConverter = new MessageDataConverter("D://PasswordsTest.csv");

    }

    @Test
    public void parsingTest() throws IOException {
        List<ClientInfo> expectedClientList = new ArrayList<>();
        expectedClientList.add(new ClientInfo("bakeryk@starfish24.ru", "bakeryK", "bakeryK"));
        expectedClientList.add(new ClientInfo("bakeryp@starfish24.ru", "bakeryP", "bakeryP"));
        List<ClientInfo> actualList = messageDataConverter.parseClientOfFile();
        assertArrayEquals(expectedClientList.toArray(), actualList.toArray());
    }

    @Test
    public void convertToMassageDataTest() {
        MessageData expectedMessageData = new MessageData("bakeryk@starfish24.ru", "Логин и пароль для доступа в систему", "sample", "sample", "Новый логин для доступа в систему: bakeryK, новый пароль: bakeryK");
        MessageData actualMessageData = messageDataConverter.convertToMessageData(new ClientInfo("bakeryk@starfish24.ru", "bakeryK", "bakeryK"));
        assertEquals(expectedMessageData, actualMessageData);
    }

}